Source: astropy-iers-data
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               pybuild-plugin-pyproject,
               python3-all,
               python3-setuptools,
               python3-setuptools-scm
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/astropy-iers-data
Vcs-Git: https://salsa.debian.org/debian-astro-team/astropy-iers-data.git
Homepage: https://github.com/astropy/astropy-iers-data
Rules-Requires-Root: no

Package: python3-astropy-iers-data
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         python3:any
Recommends: python3-astropy
Description: IERS Earth Rotation and Leap Second tables for Astropy
 This package provides IERS Earth Rotation and Leap Second tables for the
 astropy.utils.iers package.
 .
 There the following IERS data products are included:
 .
  * Bulletin A (IERS_A) is updated weekly and has historical data
    starting from 1973 and predictive data for 1 year into the
    future. It contains Earth orientation parameters x/y pole, UT1-UTC
    and their errors at daily intervals.
 .
  * Bulletin B (IERS_B) is updated monthly and has data from 1962 up to
    the time when it is generated. This file contains Earth’s
    orientation in the IERS Reference System including Universal Time,
    coordinates of the terrestrial pole, and celestial pole offsets.
 .
 The package also provides leap second data.
 .
 Note: This package is not currently meant to be used directly by
 users, and only meant to be used from the core Astropy package.
